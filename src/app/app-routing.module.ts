import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VideoItemComponent } from './video-item/video-item.component';
import { VideosListComponent } from './videos-list/videos-list.component';


const routes: Routes = [
  { path: '', component: VideosListComponent },
  { path: 'video', component: VideoItemComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
