import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss']
})
export class VideoItemComponent implements OnInit {
  state$: Observable<[]>;
  wholeState;

  constructor(private route: ActivatedRoute, private router: Router, private spinner: NgxSpinnerService) { }


  ngOnInit() {
    this.spinner.show();
    console.log(this.wholeState?.data)
    this.state$ = this.route.paramMap.pipe(
      map(() => window.history.state)
    );

    this.state$.subscribe(res => {
      this.wholeState = res;
      this.spinner.hide();
    })

  }

}
