import { Component, OnInit } from '@angular/core';
import { YoutubeServiceService } from '../youtube-service.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  styleUrls: ['./videos-list.component.scss']
})
export class VideosListComponent {
  videos = [];                // Videos Array
  playListId: string = '';    // PlayList Id
  errorMsg;                   // Error Message Holder

  constructor(private youtubeService: YoutubeServiceService, private router: Router, private spinner: NgxSpinnerService) { }
  ngOnInit() {
    this.videos = []; // For Reseting the videos array everytime you search
    this.spinner.show()
    this.youtubeService.getVideos('PLM_i0obccy3u5_3geCVMO8v9uGTeSZ1yF').subscribe((videos: any) => {
      this.videos = videos.items;
      this.spinner.hide();
      this.playListId = ''; // Clearing The Input Value After The Api Call
    },
      err => {
        this.errorMsg = 'Wrong PlayList ID'
        // UnComment The Next Line To Print Out The Error Msg
        // this.errorMsg = err.message;
        this.spinner.hide();
      })
  }
  grapVideos() {
    this.videos = []; // For Reseting the videos array everytime you search
    this.spinner.show()
    if (this.playListId) {
      this.youtubeService.getVideos(this.playListId).subscribe((videos: any) => {
        this.videos = videos.items;
        this.spinner.hide();
        this.playListId = ''; // Clearing The Input Value After The Api Call
      },
        err => {
          this.errorMsg = 'Wrong PlayList ID'
          // UnComment The Next Line To Print Out The Error Msg
          // this.errorMsg = err.message;
          this.spinner.hide();
        })
    }
  }


  videoDetails(video) {
    this.router.navigateByUrl('/video', { state: { data: video } });
  }
}
