import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class YoutubeServiceService {

  constructor(private http: HttpClient) { }


  // > Get All Videos By Playlist Id it is limited to 28 results
  getVideos(playlistId) {
    return this.http.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&maxResults=28&playlistId=' + playlistId + '&key=' + environment.apiKey)
  }
}




